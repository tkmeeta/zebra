package com.guosen.zebra.gateway.route.router;

import com.guosen.zebra.gateway.route.model.RouteInfo;
import mockit.Expectations;
import mockit.Injectable;
import mockit.Tested;
import org.junit.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.nullValue;

public class RouterTest {

    @Tested
    private Router router;

    @Injectable
    private PreciseRouter preciseRouter;

    @Injectable
    private FuzzyRouter fuzzyRouter;

    @Test
    public void testNotMatch() {
        new Expectations() {
            {
                preciseRouter.route(anyString);
                result = null;
            }
            {
                fuzzyRouter.route(anyString);
                result = null;
            }
        };

        RouteInfo routeInfo = router.route("/firstService/hello");

        assertThat(routeInfo, is(nullValue()));
    }

    @Test
    public void testMatchByPrecise() {
        RouteInfo cfgRouteInfo = RouteInfo.newBuilder().build();

        new Expectations() {
            {
                preciseRouter.route(anyString);
                result = cfgRouteInfo;
            }
        };

        RouteInfo routeInfo = router.route("/firstService/hello");

        assertThat(routeInfo, is(cfgRouteInfo));
    }

    @Test
    public void testMatchByFuzzy() {
        RouteInfo cfgRouteInfo = RouteInfo.newBuilder().build();

        new Expectations() {
            {
                preciseRouter.route(anyString);
                result = null;
            }
            {
                fuzzyRouter.route(anyString);
                result = cfgRouteInfo;
            }
        };

        RouteInfo routeInfo = router.route("/firstService/hello");

        assertThat(routeInfo, is(cfgRouteInfo));
    }

}
