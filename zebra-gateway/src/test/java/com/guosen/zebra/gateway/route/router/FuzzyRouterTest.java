package com.guosen.zebra.gateway.route.router;

import com.guosen.zebra.gateway.route.cache.UrlPrefixRouteCache;
import com.guosen.zebra.gateway.route.model.RouteInfo;
import mockit.Expectations;
import mockit.Mocked;
import mockit.Tested;
import org.junit.Test;

import java.util.HashMap;
import java.util.Map;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;

public class FuzzyRouterTest {

    @Tested
    private FuzzyRouter fuzzyRouter;

    @Mocked
    private UrlPrefixRouteCache urlPrefixRouteCache;

    /**
     * 测试完全匹配不上的情况
     */
    @Test
    public void testNotMatch() {
        RouteInfo routeInfo = RouteInfo.newBuilder()
                .urlPrefix("/secondService")
                .serviceName("com.guosen.zebra.sample.SecondService")
                .build();

        Map<String, RouteInfo> urlPrefixRouteInfoMap = new HashMap<>();
        urlPrefixRouteInfoMap.put("/secondService", routeInfo);

        new Expectations() {
            {
                urlPrefixRouteCache.getUrlPrefixRouteInfo();
                result = urlPrefixRouteInfoMap;
            }
        };

        RouteInfo resultRouteInfo = fuzzyRouter.route("/firstService/hello");

        assertThat(resultRouteInfo, is(nullValue()));
    }

    /**
     * 测试匹配上前缀但是找不到方法的情况
     */
    @Test
    public void testMatchPrefixButNoMethod() {
        RouteInfo routeInfo = RouteInfo.newBuilder()
                .urlPrefix("/secondService")
                .serviceName("com.guosen.zebra.sample.SecondService")
                .build();

        Map<String, RouteInfo> urlPrefixRouteInfoMap = new HashMap<>();
        urlPrefixRouteInfoMap.put("/secondService", routeInfo);

        new Expectations() {
            {
                urlPrefixRouteCache.getUrlPrefixRouteInfo();
                result = urlPrefixRouteInfoMap;
            }
        };

        // 只传递了前缀
        RouteInfo resultRouteInfo = fuzzyRouter.route("/secondService/");

        assertThat(resultRouteInfo, is(nullValue()));
    }

    /**
     * 测试匹配上的情况
     */
    @Test
    public void testMatch() {
        RouteInfo routeInfo = RouteInfo.newBuilder()
                .urlPrefix("/secondService")
                .serviceName("com.guosen.zebra.sample.SecondService")
                .build();

        Map<String, RouteInfo> urlPrefixRouteInfoMap = new HashMap<>();
        urlPrefixRouteInfoMap.put("/secondService", routeInfo);

        new Expectations() {
            {
                urlPrefixRouteCache.getUrlPrefixRouteInfo();
                result = urlPrefixRouteInfoMap;
            }
        };

        RouteInfo resultRouteInfo = fuzzyRouter.route("/secondService/hello");

        assertThat(resultRouteInfo, is(notNullValue()));
        assertThat(resultRouteInfo.getMethod(), is("hello"));
    }
}
