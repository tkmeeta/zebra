package com.guosen.zebra.gateway.route.cache;

import com.guosen.zebra.gateway.route.model.RouteInfo;
import mockit.Tested;
import org.junit.Test;

import java.util.HashMap;
import java.util.Map;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.nullValue;

public class UrlPrefixRouteCacheTest {

    @Tested
    private UrlPrefixRouteCache urlPrefixRouteCache;

    @Test
    public void testUpdate() {

        // 初始化
        String firstService = "firstService";
        RouteInfo firstServiceRouteInfo = RouteInfo.newBuilder()
                .serviceName("com.guosen.zebra.sample.FirstService")
                .urlPrefix("/firstService")
                .build();

        Map<String, RouteInfo> firstServiceRouteInfoMap = new HashMap<>();
        firstServiceRouteInfoMap.put("/firstService", firstServiceRouteInfo);

        urlPrefixRouteCache.update(firstService, firstServiceRouteInfoMap);

        String secondService = "secondService";
        RouteInfo secondServiceRouteInfo = RouteInfo.newBuilder()
                .serviceName("com.guosen.zebra.sample.SecondService")
                .urlPrefix("/secondService")
                .build();

        Map<String, RouteInfo> secondServiceRouteInfoMap = new HashMap<>();
        secondServiceRouteInfoMap.put("/secondService", secondServiceRouteInfo);

        urlPrefixRouteCache.update(secondService, secondServiceRouteInfoMap);

        // 更新
        RouteInfo newSecondServiceRouteInfo = RouteInfo.newBuilder()
                .serviceName("com.guosen.zebra.sample.SecondService")
                .urlPrefix("/newSecondService")
                .build();

        Map<String, RouteInfo> newSecondServiceRouteInfoMap = new HashMap<>();
        newSecondServiceRouteInfoMap.put("/newSecondService", newSecondServiceRouteInfo);

        urlPrefixRouteCache.update(secondService, newSecondServiceRouteInfoMap);

        Map<String, RouteInfo> routeInfoMap = urlPrefixRouteCache.getUrlPrefixRouteInfo();

        // 验证
        assertThat(routeInfoMap.size(), is(2));

        assertThat(routeInfoMap.get("/firstService"), is(firstServiceRouteInfo));
        assertThat(routeInfoMap.get("/newSecondService"), is(newSecondServiceRouteInfo));
        assertThat(routeInfoMap.get("/secondService"), is(nullValue()));
    }
}
