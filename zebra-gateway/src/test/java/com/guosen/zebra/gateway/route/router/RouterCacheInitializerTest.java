package com.guosen.zebra.gateway.route.router;

import com.guosen.zebra.gateway.dao.RouteConfigDao;
import com.guosen.zebra.gateway.route.cache.ConcreteUrlRouteCache;
import com.guosen.zebra.gateway.route.cache.RouterCacheInitializer;
import com.guosen.zebra.gateway.route.cache.UrlPrefixRouteCache;
import mockit.*;
import org.junit.Test;

import java.util.Map;

public class RouterCacheInitializerTest {

    @Tested
    private RouterCacheInitializer routerCacheInitializer;

    @Injectable
    private RouteConfigDao routeConfigDao;

    @Mocked
    private ConcreteUrlRouteCache concreteUrlRouteCache;

    @Mocked
    private UrlPrefixRouteCache urlPrefixRouteCache;

    @Test
    public void testNoConfig() {
        new Expectations() {
            {
                routeConfigDao.getConfigs();
                result = null;
            }
        };

        routerCacheInitializer.init();

        new Verifications() {
            {
                concreteUrlRouteCache.update(anyString, (Map)any);
                times = 0;
            }
            {
                urlPrefixRouteCache.update(anyString, (Map)any);
                times = 0;
            }
        };
    }

}
