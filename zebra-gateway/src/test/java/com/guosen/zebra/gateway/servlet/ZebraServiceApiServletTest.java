package com.guosen.zebra.gateway.servlet;

import com.alibaba.fastjson.JSONObject;
import com.guosen.zebra.gateway.exception.RouteNotFound;
import com.guosen.zebra.gateway.exception.ServiceNotFound;
import com.guosen.zebra.gateway.handler.ZebraServiceHandler;
import com.guosen.zebra.gateway.util.HttpRequestUtil;
import mockit.*;
import org.junit.Test;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

public class ZebraServiceApiServletTest {
    @Tested
    private ZebraServiceApiServlet zebraServiceApiServlet;

    @Injectable
    private ZebraServiceHandler zebraServiceHandler;

    @Mocked
    private HttpRequestUtil httpRequestUtil;

    @Mocked
    private HttpServletRequest req;

    @Mocked
    private HttpServletResponse resp;

    @Test
    public void testRouteNotFound() throws ServletException, IOException {
        JSONObject requestParameters = new JSONObject();
        Map<String, String> headers = new HashMap<>();
        String originalRequestURI = "/api/firstService/sayHello";

        new Expectations() {
            {
                req.getRequestURI();
                result = originalRequestURI;
            }
            {
                HttpRequestUtil.getJSONParam(req);
                result = requestParameters;
            }
            {
                HttpRequestUtil.getHeader(req);
                result = headers;
            }
            {
                try {
                    zebraServiceHandler.handleRequest("/firstService/sayHello", requestParameters, headers);
                } catch (RouteNotFound routeNotFound) {
                    routeNotFound.printStackTrace();
                } catch (ServiceNotFound serviceNotFound) {
                    serviceNotFound.printStackTrace();
                }

                result = new RouteNotFound();
            }
        };

        zebraServiceApiServlet.service(req, resp);

        new Verifications() {
            {
                resp.sendError(HttpServletResponse.SC_NOT_FOUND, "Route not found of URI : /api/firstService/sayHello");
                times = 1;
            }
        };
    }

    @Test
    public void testServiceNotFound() throws IOException, ServletException {
        JSONObject requestParameters = new JSONObject();
        Map<String, String> headers = new HashMap<>();
        String originalRequestURI = "/api/firstService/sayHello";

        new Expectations() {
            {
                req.getRequestURI();
                result = originalRequestURI;
            }
            {
                HttpRequestUtil.getJSONParam(req);
                result = requestParameters;
            }
            {
                HttpRequestUtil.getHeader(req);
                result = headers;
            }
            {
                try {
                    zebraServiceHandler.handleRequest("/firstService/sayHello", requestParameters, headers);
                } catch (RouteNotFound routeNotFound) {
                    routeNotFound.printStackTrace();
                } catch (ServiceNotFound serviceNotFound) {
                    serviceNotFound.printStackTrace();
                }

                result = new ServiceNotFound();
            }
        };

        zebraServiceApiServlet.service(req, resp);

        new Verifications() {
            {
                resp.sendError(HttpServletResponse.SC_NOT_FOUND, "Service not found of URI : /api/firstService/sayHello");
                times = 1;
            }
        };
    }

    @Test
    public void testServiceSuccessfully() throws ServletException, IOException {
        JSONObject requestParameters = new JSONObject();
        Map<String, String> headers = new HashMap<>();
        String originalRequestURI = "/api/firstService/sayHello";

        new Expectations() {
            {
                req.getRequestURI();
                result = originalRequestURI;
            }
            {
                HttpRequestUtil.getJSONParam(req);
                result = requestParameters;
            }
            {
                HttpRequestUtil.getHeader(req);
                result = headers;
            }
            {
                JSONObject resultJson = new JSONObject();
                resultJson.put("hello", "world");

                try {
                    zebraServiceHandler.handleRequest("/firstService/sayHello", requestParameters, headers);
                } catch (RouteNotFound routeNotFound) {
                    routeNotFound.printStackTrace();
                } catch (ServiceNotFound serviceNotFound) {
                    serviceNotFound.printStackTrace();
                }

                result = resultJson;
            }
        };

        zebraServiceApiServlet.service(req, resp);

    }
}
