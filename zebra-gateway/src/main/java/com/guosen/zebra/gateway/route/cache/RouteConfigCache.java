package com.guosen.zebra.gateway.route.cache;

import com.guosen.zebra.gateway.route.model.RouteConfig;

import java.util.Collections;
import java.util.List;

/**
 * 路由配置缓存，用于缓存更新的对比。
 */
public class RouteConfigCache {

    private static final RouteConfigCache INSTANCE = new RouteConfigCache();

    /**
     * 旧的路由配置
     */
    private List<RouteConfig> oldConfigs = Collections.emptyList();

    private RouteConfigCache(){}

    public static RouteConfigCache getInstance() {
        return INSTANCE;
    }

    public List<RouteConfig> getOldConfigs() {
        return oldConfigs;
    }

    public synchronized void setOldConfigs(List<RouteConfig> oldConfigs) {
        this.oldConfigs = oldConfigs;
    }
}
